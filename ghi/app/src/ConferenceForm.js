import React, {useEffect, useState,} from 'react';



function ConferenceForm(props){

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data={};

    data.name =name;
    data.description = description;
    data.max_presentations= presentations;
    data.max_attendees= attendees;
    data.starts = startDate;
    data.ends = endDate;
    data.location= location;
    console.log(data);


    const conferenceUrl = 'http://localhost:8000/api/conferences/';
        
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
        const newConference = await response.json();
        console.log(newConference);

    setName('');
    setStartDate('');
    setEndDate('');
    setDescription('');
    setPresentations('');
    setAttendees('');
    
    } 
    }


    const [name, setName]=useState('');
    const handleNameChange= (event)=> {
        const value = event.target.value;
        setName(value);
    }
    const [startDate, setStartDate]=useState('');
    const handleStartDateChange= (event)=> {
        const value = event.target.value;
        setStartDate(value);
    }
    const [endDate, setEndDate]=useState('');
    const handleEndDateChange= (event)=> {
        const value = event.target.value;
        setEndDate(value);
    }

    const [description, setDescription]=useState('');
    const handleDescriptionChange= (event)=> {
        const value = event.target.value;
        setDescription(value);
    }
    const [presentations, setPresentations]=useState('');
    const handlePresentationsChange= (event)=> {
        const value = event.target.value;
        setPresentations(value);
    }
    
    const [attendees, setAttendees]=useState('');
    const handleAttendeesChange= (event)=> {
        const value = event.target.value;
        setAttendees(value);
    }
    const [location, setLocation]=useState('');
    const handleLocationChange= (event)=> {
        const value = event.target.value;
        setLocation(value);
    }

    const [locations, setLocations]= useState([]);
    const fetchData = async () =>{
        const url = 'http://localhost:8000/api/locations/';
  
        const response = await fetch(url);
  
        if (response.ok) {
        const data = await response.json();
        console.log(data);
        setLocations(data.locations);
        }
    }

    useEffect(() => {fetchData();}, []);

    return(
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
                <input onChange={handleNameChange}placeholder="Name" required type="text" id="name" name="name" className="form-control"/>
                <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleStartDateChange} placeholder="starts" required type="date" id="starts" name="starts" className="form-control"/>
                <label htmlFor="starts">Start Date</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleEndDateChange} placeholder="ends" required type="date" name="ends" id="ends" className="form-control"/>
                <label htmlFor="ends">End Date</label>
            </div>
            <div className="mb-4">
                <textarea onChange={handleDescriptionChange} className="form-control" placeholder="write your description here" id="description" name="description"></textarea>
                <label htmlFor="description">Conference Description</label>
            </div>
            <div className="mb-3">
                <input onChange={handlePresentationsChange} placeholder="max_presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                <label htmlFor="max_presentations">Number of Presentations</label>
            </div>
            <div className="mb-3">
                <input onChange={handleAttendeesChange} placeholder="max_attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Number of Attendees</label>
            </div>
            <div className="mb-3">
                <select onChange={handleLocationChange} required id="locations" name="location" className="form-select">
                <option value='' >Choose a Location</option>
                {locations.map(location => {
                    return (
                        <option key= {location.id} value={location.id}>
                            {location.name}
                        </option>
                    )
                })}
                </select>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
            </div>
            </div>
        </div>
        
    )

}

export default ConferenceForm;
