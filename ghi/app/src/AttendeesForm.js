import React, {useEffect, useState,} from 'react';


function AttendeesForm(props){

    const [conferences, setConferences] = useState([]);
    const [name, setName] = useState('');
    const [conference, setConference] = useState('');
    const [hasSignedUp, setHasSignedUp] = useState(false);
    

    // const [spinnerClasses, setSpinnerClasses] = useState('d-flex justify-content-center mb-3');
    // const [dropdownClasses, setDropDownClasses] = useState("form-select d-none");
    
    
    const fetchData = async () =>{
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setConferences(data.conferences);

        }
    }
    useEffect(() => {fetchData();}, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        
        const data = {};
        data.name = name;
        data.conference = conference;
        data.email = email;
        console.log(data);

        const attendeeUrl = 'http://localhost:8001/api/attendees/';

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const attendeeResponse = await fetch(attendeeUrl, fetchConfig);
        if (attendeeResponse.ok){
            setName('');
            setConference('');
            setEmail('');
            setHasSignedUp(true);
        }
    }
    const handleConferenceChange= (event)=> {
        const value = event.target.value;
        setConference(value);
    } 
    const handleNameChange= (event)=> {
        const value = event.target.value;
        setName(value);
    }
    const [email, setEmail]=useState('');
    const handleEmailChange= (event)=> {
        const value = event.target.value;
        setEmail(value);
    }
    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (conferences.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (hasSignedUp) {
      messageClasses = 'alert alert-success mb-0';
      formClasses = 'd-none';
    }  

    return (
        <div className="my-5 container">
        <div className="row">
        <div className="col col-sm-auto">
            <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg"/>
        </div>
        <div className="col">
            <div className="card shadow">
                <div className="card-body">
                <form className={formClasses} onSubmit={handleSubmit} id="create-attendee-form">
                <h1 className="card-title">It's Conference Time!</h1>
                <p className="mb-3">
                    Please choose which conference
                    you'd like to attend.
                </p>
                <div className={spinnerClasses}  id="loading-conference-spinner">
                    <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                    </div>
                </div>
                <div className="mb-3">
                <select onChange={handleConferenceChange} name="conference" value={conference} id="conference" className={dropdownClasses} required>
                    <option value="">Choose a conference</option>
                    {conferences.map((conference, idx) => {
                        // console.log('array',arr);
                        return (
                            <option key={idx} value={conference.href}>
                                {conference.name}
                            </option>
                            )
                    })}
                </select>
                </div>
                <p className="mb-3">
                    Now, tell us about yourself.
                </p>
                <div className="row">
                    <div className="col">
                        <div className="form-floating mb-3">
                        <input onChange={handleNameChange} required placeholder="Your full name" type="text" id="name" name="name" value={name} className="form-control"/>
                        <label htmlFor="name">Your full name</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                        <input onChange={handleEmailChange} required placeholder="Your email address" type="email" id="email" name="email" value={email} className="form-control"/>
                        <label htmlFor="email">Your email address</label>
                        </div>
                    </div>
                </div>
                <button className="btn btn-lg btn-primary">I'm going!</button>
                </form>
            <div className={messageClasses} id="success-message">
                Congratulations! You're all signed up!
            </div>
        </div>
        </div>
        </div>
        </div>
        </div>
    )

}

export default AttendeesForm;
