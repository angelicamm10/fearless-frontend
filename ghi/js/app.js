function createCard(name, description, pictureUrl, location, confDate, confDateEnd) {
    return `
    <div class = "col-4">
    <div class= "card shadow ">  
    <div class="card h-100">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>    
        <div class = "card-footer">
        <small class="text-muted">${confDate} - ${confDateEnd}</small></div>
        </div>
    
    </div>    
    </div>
    </div>
    </div>
    
    `;
  }





window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    try{
        const response = await fetch(url);
        // throw new Error();
        if(!response.ok){
            console.log("response not okay")
        } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const title = details.conference.name;
              const description = details.conference.description;
              const location = details.conference.location.name;
              const pictureUrl = details.conference.location.picture_url;
              const conferenceStarts = new Date(details.conference.starts);
              const confDate =conferenceStarts.toLocaleDateString();
              const conferenceEnds = new Date(details.conference.ends);
              const confDateEnd= conferenceEnds.toLocaleDateString();

              const html = createCard(title, description, pictureUrl, location, confDate, confDateEnd);
              const column = document.querySelector('.row');
              column.innerHTML += html;
            }
        }
     
        }
    } catch (e) {
        console.error(e);
        const alertPlaceholder = document.getElementById('liveAlertPlaceholder')
      

        function alert(message, type) {
        const wrapper = document.createElement('div')
        wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

        alertPlaceholder.append(wrapper)
        }

        if (alertPlaceholder) {
            alert('There was a problem loading, please try again!', 'warning')
    
        }
    }
});
